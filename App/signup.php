<?php

#Chargment de l'autoload
require_once './vendor/autoload.php';
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\RetryableHttpClient;

#Récupération du client http Symfony
$client = new RetryableHttpClient(HttpClient::create(["verify_peer"=>false,"verify_host"=>false]));
#Requete à notre API pour récupérer les articless
$response = $client->request('GET', 'http://localhost:8000/api/users');

#Debbugage de la requete du navigateur
#dump($response->getContent());

#Récupeartion des articles
$users = $response->toArray();
require_once 'header.php';
?>


<div class="container" style="padding-top: 90px">
    <h1>Créer votre compte</h1>
    <meta charset="utf-8">
    <form name="formAjout" action="" method="post" class="addForm" >
        <fieldset>
            <legend>Entrer les données nécessaires a la création du compte </legend>
            <div class="row">
                <div class="col-7">
                    <p> <label>Nom : </label>  <input type="text"  name="lastname"  class="form-control" required/></p>
                </div>
                <div class="col-7">
                    <p> <label>Prenom : </label>  <input type="text" name="firstname"  class="form-control" required/></p>
                </div>
                <div class="col-7">
                    <p> <label>Email : </label>  <input type="email"   class="form-control" required/></p>
                </div>
                <div class="col-7">
                    <p>  <label>Mot de passe :</label>  <input type="password"  name="pass" size="20" class="form-control" required/></p>
                </div>
                <div class="col-7">
                    <p>  <label>Répéter mot de passe :</label> <input type="password"   name="pass_t" size="20" class="form-control" required/></p>
                </div>
                <div class="col-7">
                    <p><label>Téléphone :</label> <input type="text"  name="phone" size="10" class="form-control" required/></p>
                </div>
                <div class="col-7">
                    <p> <label> Date d'anniversaire </label> <input type="date"  name="birthday" size="20" class="form-control" required/></p>
                </div>
            </div>
        </fieldset>
        <button type="submit" class="btn btn-primary ">Enregistrer</button>
        <button type="reset" class="btn btn-secondary">Annuler</button>
    </form>
</div>


<?php
require_once 'footer.php';
?>
