<?php

#Chargment de l'autoload
require_once './vendor/autoload.php';
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\RetryableHttpClient;

#Récupération du client http Symfony
$client = new RetryableHttpClient(HttpClient::create(["verify_peer"=>false,"verify_host"=>false]));
#Requete à notre API pour récupérer les articless
$response = $client->request('GET', 'http://localhost:8000/api/users');

#Debbugage de la requete du navigateur
#dump($response->getContent());

#Récupeartion des articles
$users = $response->toArray();
require_once 'header.php';
?>


<div class="container" align="center" style="padding-top: 90px">
    <br><legend>Paiement</legend>
    <form name="formAjout" action="" method="post" onSubmit="return valider()" >
        <fieldset>
            <div class=row>
                <div class="col-6 col-md-4">
                </div>
                <div class="col-6 col-md-4">
                    <div class="control-group"> <div class="controls">
                            <input type="int" class="form-control" id="num" name="num" aria-describedby="loginHelp" placeholder="NUMERO DE CARTE" size="16">
                        </div>
                    </div><p>
                    <div class="control-group">
                        <div class="controls">
                            <input type="int" class="form-control" id="cvv2" name="cvv2" placeholder="CVV2" size="3">
                        </div>
                    </div></p>
                    <div class="control-group">
                        <div class="controls">
                            <input type="text" class="form-control" id="titulaire" name="titulaire" placeholder="TITULAIRE" >
                        </div>
                    </div></p>
                    <div class="control-group">
                        <div class="controls">
                            <input type="int" class="form-control" name="date" placeholder="DATE D'EXPIRATION MM/YYYY" size="7">
                        </div>

                    </div></p>
                </div>
                <div class="col-6 col-md-4">
                </div>
            </div>
        </fieldset>

        <button type="submit" class="btn btn-primary">Payer</button>
        <span class="separator">&nbsp;&nbsp;</span>
    </form>

    <p><strong>VISA</strong></p><p><strong>MASTERCARD</strong></p><p><strong>AMEX</strong></p><p><strong>APPLE PAY</strong></p><p><strong>GOOGLE PAY</strong></p>
</div>










<?php
require_once 'footer.php';
?>

