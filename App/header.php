<!DOCTYPE html>
<html lang="en">
<head>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="app.css" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rodar</title>
    <script src = "sw.js"> </script>
    <script src = "index.js"> </script>
    <link rel="manifest" href="./manifest.webmanifest">
    <link rel="icon" type="image/png" href="img/ICONE-OFF.png" />
</head>

<header class="header">

    <input class="menu-btn" type="checkbox" id="menu-btn" />
    <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>

    <ul class="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="signup.php">Inscription</a></li>
        <li><a href="signin.php">Connexion</a></li>
        <li><a href="trajet.php">Trajet</a> </li>
        <li><a href="paiement.php">Paiement</a> </li>
        <li><a href="promotion.php">Promotions</a> </li>
    </ul>

</header>

<body>

