<?php

#Chargment de l'autoload
require_once './vendor/autoload.php';
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\RetryableHttpClient;

#Récupération du client http Symfony
$client = new RetryableHttpClient(HttpClient::create(["verify_peer"=>false,"verify_host"=>false]));
#Requete à notre API pour récupérer les articless
$response = $client->request('GET', 'http://localhost:8000/api/users');

#Debbugage de la requete du navigateur
#dump($response->getContent());

#Récupeartion des articles
$users = $response->toArray();
require_once 'header.php';
?>


<div class="container" align="center" style="padding-top: 90px">
    <br><legend>Connexion</legend>
    <form name="formAjout" action="" method="post" onSubmit="return valider()" >
        <fieldset>
            <div class=row>
                <div class="col-6 col-md-4">
                </div>
                <div class="col-6 col-md-4">
                    <div class="control-group"> <div class="controls">
                            <input type="email" class="form-control" id="email" name="email" aria-describedby="loginHelp" placeholder="Entrer Email" size="10">
                        </div>
                    </div><p>
                    <div class="control-group">
                        <div class="controls">
                            <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password" size="10">
                        </div>
                    </div></p>
                </div>
                <div class="col-6 col-md-4">
                </div>
            </div>
        </fieldset>
        <button type="submit" class="btn btn-primary">Suivant</button>
        <span class="separator">&nbsp;&nbsp;</span>
    </form>

</div>

<?php
require_once 'footer.php';
?>

